# /bin/bash/

################
#>MAINTAINER: David Halbani
#>DISCRIPTON: ICE MACSPOOFER for more Traffic on RAIL
#>Version: 0.0.1
###############

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
SPOOFSAVE=~/.macspoof/conf/macspoofing_save
MACADRESS=~/macspoof/conf/macadress_list
INTERFACE=en0
LOG_PATH=~/.macspoof/log/
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
#>Shell CSS
lin=______________________________
li=---------------
hr=
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
if [[! -d ${SPOOFSAVE} ]]; then
          mkdir ~/.macspoof/
          mkdir ~/.macspoof/conf
          echo "
02:80:D1:BA:99:AB
02:02:02:01:01:01
02:a3:41:67:67:67
02:99:99:93:33:33
02:43:43:43:43:55
02:83:83:83:31:11
          " > ${MACADRESS}
fi
if [[! -d ${SPOOFSAVE} ]]; then
          echo "save your Real MAC Adress"
            ifconfig ${INTERFACE} | grep ether > ${SPOOFSAVE}
            sed -i -e "s/ether//" ${SPOOFSAVE}
          echo $hr
          echo $li
          echo $hr
            echo "echo 'the original Mac Adress:"
            cat ${SPOOFSAVE}
          echo $lin
  fi

	        echo
            clear
	          echo "Which MAC do you want to use"
	          echo "   1) 02:80:D1:BA:99:AB"
	          echo "   2) 02:02:02:01:01:01"
	          echo "   3) 02:a3:41:67:67:67"
	          echo "   4) 02:99:99:93:33:33"
	          echo "   5) 02:43:43:43:43:55"
            echo "   6) 02:83:83:83:31:11"
            echo "   7) exit spoofing"
	          read -p "MAC [1-5]: "  MAC
	        echo
	            case $MAC in
		            1)
                  clear
		              echo $lin
                    echo "spoof the macadress to: 02:80:D1:BA:99:AB"
                    sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 1 && NR <= 1' ${MACADRESS})
                  echo $hr
                  echo $li
                  echo $hr
                    clear
                    echo "check mac"
                    echo 'new'
                    ifconfig ${INTERFACE} | grep ether
                    echo 'old'
                    cat ${SPOOFSAVE}
                  echo $hr
                  echo $li
                  echo $hr
                    clear
                    echo "restart Interface"
                    sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                 echo $hr
                 echo $li
                 echo $hr
                    clear
                    echo "finish with a moment to reconnect the wifi and have fun ;P"
                 echo $lin

              ;;
		          2)
                clear
                echo $lin
                  echo "spoof the macadress to: 02:02:02:01:01:01"
                  sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 2 && NR <= 2' ${MACADRESS})
		            echo $hr
                echo $li
                echo $hr
                  clear
                  echo "check mac"
                  echo 'new'
                  ifconfig ${INTERFACE} | grep ether
                echo 'old'
                  clear
                  cat ${SPOOFSAVE}
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "restart Interface"
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "finish with a moment to reconnect the wifi and have fun ;P"
                echo $lin
              ;;
		          3)
                clear
                echo $lin
                  echo "spoof the macadress to: 02:a3:41:67:67:67"
                  sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 3 && NR <= 3' ${MACADRESS})
		            echo $hr
                echo $li
                echo $hr
                  clear
                  echo "check mac"
                  echo 'new'
                  ifconfig ${INTERFACE} | grep ether
                  echo 'old'
                  cat ${SPOOFSAVE}
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "restart Interface"
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "finish with a moment to reconnect the wifi and have fun ;P"
                echo $lin
              ;;
		          4)
                clear
                echo $lin
                  echo "spoof the macadress to: 02:99:99:93:33:33"
                  sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 4 && NR <= 4' ${MACADRESS})
		            echo $hr
                echo $li
                echo $hr
                  clear
                  echo "check mac"
                  echo 'new'
                  ifconfig ${INTERFACE} | grep ether
                  echo 'old'
                  cat ${SPOOFSAVE}
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "restart Interface"
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "finish with a moment to reconnect the wifi and have fun ;P"
                echo $lin
              ;;
		          5)
                echo $lin
                  clear
                  echo "spoof the macadress to: 02:43:43:43:43:55"
                  sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 5 && NR <= 5' ${MACADRESS})
		            echo $hr
                echo $li
                echo $hr
                  clear
                  echo "check mac"
                  echo 'new'
                  ifconfig ${INTERFACE} | grep ether
                  echo 'old'
                  cat ${SPOOFSAVE}
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "restart Interface"
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "finish with a moment to reconnect the wifi and have fun ;P"
                echo $lin
              ;;
              6)
                clear
                echo $lin
                  echo "spoof the macadress to: 02:83:83:83:31:11"
                  sudo ifconfig ${INTERFACE} ether $(awk 'NR >= 6 && NR <= 6' ${MACADRESS})
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "check mac"
                  echo 'new'
                  ifconfig ${INTERFACE} | grep ether
                  echo 'old'
                  cat ${SPOOFSAVE}
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "restart Interface"
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "finish with a moment to reconnect the wifi and have fun ;P"
                echo $lin
              ;;
              7)
                echo $lin
                  clear
                  echo "Remove all data"
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "Remove dir ${SPOOFSAVE}"
                  rm -rf ${SPOOFSAVE}
                  echo "Remove dir ${MACADRESS}"
                  rm -rf ${MACADRESS}
                echo $hr
                echo $li
                  clear
                  echo "I Bring you back Real MAC Adress"
                  sudo ifconfig ${INTERFACE} ether $(cat ${SPOOFSAVE})
                  sudo ifconfig ${INTERFACE} down && sudo ifconfig ${INTERFACE} up
                  sleep 2
                echo $hr
                echo $li
                echo $hr
                  clear
                  echo "Networkcart Restart"
                  sudo ifconfig ${INTERFACE} down
                  sudo ifconfig ${INTERFACE} up
                  sleep 1
                echo $hr
                echo $li
                echo $hr
                echo $hr
                  clear
                  echo "finish you have now your real mac"
                echo $lin
              ;;
	        esac
